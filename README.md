
```
Ajit Pawar
Team: GameUp!
Assignment 3, CSC490
University of Toronto
Summer 2015
```



# Part 1
### Architecture
___
GameUp! is a multi-sided platform that connects social gamers to game organizers. For gamers, it allows them to discover new events, buy tickets online and enroll in tournaments online. For event organizers, it simplifies creating tournament brackets, provides an online ticketing system and gives them platform to promote their events to a niche market.  The major technologies behind GameUp! are:

* Parse (back-end)
* AngularJS (front-end)
* Stripe (payment)

Here is our project repo: [https://github.com/GameUp](https://github.com/GameUp)

### My contribution
___
#### Major contribution: Stripe integration
I was fully responsible for integrating Stripe with Parse. The two key aspects of this integration are:

* Stripe.js
* Parse Cloud

Here is the flow of events for payments:

* User clicks "Buy" button
* The credit-card form (card number, expiry, cvc) gets submitted to Parse Cloud

```
// Controller for "Buy" button
$scope.chargeStripeCardOfUser = function(){

	// Get form
	var customerID = $scope.user.stripe.customerID;
	var recipientID = $scope.user.stripe.recipientID;
	var paid = $scope.user.stripe.paid;
	var amount = $scope.user.stripe.amount;

	// Submit form to Parse Cloud
	Parse.Cloud.run('chargeStripeCardOfUser', {
		customerID: customerID,
		amount: amount
	}
```

* Parse Cloud calls Stripe to create a new token

```
// Call Stripe and create token
Parse.Cloud.define("addStripeCardToUser", function(request, response){
	Parse.Cloud.httpRequest({
	    method : 'POST',
	    url : 'https://api.stripe.com/v1/tokens',
	    headers : {
	        'Authorization' : 'Bearer '+STRIPE_SECRET_KEY
	    },
	    body : {
	        "card[number]" : request.params.payload.number,
	        "card[exp_month]" : request.params.payload.month,
	        "card[exp_year]" : request.params.payload.year,
	        "card[cvc]" : request.params.payload.cvc
	    },
	    success : function(httpResponse) {
	        response.success(httpResponse.data.card.id);	// return token
	    },
	    error : function(err) {
	        response.error(err);
	    }
	});
});
```

* Stripe returns a token
* Token gets stored in Parse (only ```ID``` is stored, not the actual token)

![](img1.png)

* Use the token to charge the customer

```
// Create a new charge against this customer
Parse.Cloud.define("chargeStripeCardOfUser", function(request, response)
{
	Stripe.Charges.create({
	  amount: request.params.amount,
	  currency: "usd",
	  customer: request.params.customerID
	},
	{
	  success: function(httpResponse) {
	    response.success(httpResponse);
	  },
	  error: function(err) {
	    response.error(err);
	  }
	});

});
```

Done!


**Here is all the Stripe related code I wrote:**

[Parse Cloud functions](https://github.com/GameUp/GameUp.github.io/commit/12b10ccdc3614b04aaeb70c276db4e72f2351388)

[Controller that calls Parse Cloud functions](https://github.com/GameUp/GameUp.github.io/commit/161f8794aecfd5fc85656288644b022b0a00909f) 

[Angular Service for storing tokens in Parse](https://github.com/GameUp/GameUp.github.io/blob/stripe-payment/app/services/tickets.js)

#### Minor contributions: Login, Sign Up, Profile
Following are some of the Angular controllers I wrote:

[Login Controller ](https://github.com/GameUp/GameUp.github.io/commit/e0bd28fa3fda679721f7a1992d17ec63e1028487) 

[Signup Controller](https://github.com/GameUp/GameUp.github.io/commit/d53835cba718d81ec56250ce8ebd280966ea593a) 

[Profile Controller](https://github.com/GameUp/GameUp.github.io/commit/22875407140c4adb464439693b66b213e4dfccf2)

#### Other contributions: Parse Architecture
I was partially responsible for setting up the classes in Parse and making decisions regarding how/which data will be stored. For example, in the ```Tickets``` class, ```User``` and ```Events``` columns were initially strings. I replaced it and changed it to Pointers instead.

![](img2.png)

The reason here was to make it faster and more efficient to retrieve all ticket-related data without having to manually iterate over ```User``` and ```Event``` class everytime. For example, if the user was stored as a string, then we would need to take that string and search the entire User class to find the User object. This is a wasteful and naive search algorithm. Use **Pointers** instead!!

### Key Technical Decisions
___
An important technical decision I made was to implement Stripe on the Parse Cloud instead of client-side. The reason was security and efficiency. I did not want to expose my payment processing logic to the user. Also, running the code on Parse servers meant faster response time than some client who may be using a mobile device. Let Parse do the heavy lifting!

### My Learning Experience
___

#### What I found easy
* Parse made back-end super easy.
* Stripe has a simple & powerful API that made implementing payments relatively easy

#### What I found challenging
* Parse has a Cloud Module for Stripe such that Stripe actions can be natively performed in Parse Cloud. The problem is, Parse is using an outdated version of Stripe. Also, the documentation is basically non-existent. So, a lot of things required trial-and-error. For example, creating charges according to Stripe documentation is  ``` stripe.charges.create()``` but in Parse Cloud Module it is ```Stripe.Charges.create()```.


* Parse Cloud Code cannot be debugged since the code is run in their cloud. So, the only way to debug it was to log the output and then look at the logs to see what happened. This is inefficient and slow. I prefer being able to insert ```breakpoints``` in my code and stepping through them. Unfourtantely, this cannot be done with Cloud Code.

#### Mistakes
One mistake I made was to not notice that one of Stripe's functionality was deprecated.

![](img3.png)

Declaring a ```Recipient``` allows you to charge the customer (ie. gamer) and send the money directly to the recipient (ie. game store) while automatically deducting your own fees in the process.

Since this functionality was deprecated, I had to re-write my code so as to manually deduct our fees first and then pay the balance to the recipient (ie. game store). Had I not fixed that mistake, we would have basically made no money despite selling tickets :P


### Next steps for me
___
My next step to expand the Stripe integration to include Refunds, Discounts, Invoices and Subscription Plans.


### Conclusion
___
Overall, it was an amazing learning experience for me. Learning Parse, Stripe and Angular in a hands-on way was a joy. Thank you Liam and Joey for all the technical help and guidance :)

# Part 2
For my code review, I picked a Parse related question since it was something I learnt recently and thought it would be a challenge + learning experience to review:
[Code Review](https://codereview.stackexchange.com/questions/98418/parse-com-simple-query-login-and-display-account-balance)